import React from 'react';
import {
  SafeAreaView,
  StyleSheet, Alert,
  StatusBar, ImageBackground, PermissionsAndroid,
} from 'react-native';
import BaseScreen from './screens/BaseScreen';
import {inject, observer} from 'mobx-react';
import AuthScreen from './screens/AuthScreen';
import {ActivityIndicator, Card} from 'react-native-paper';
import storage from './utils/storage';
import {size} from './utils';
import firebase from '@react-native-firebase/app';
import messaging from '@react-native-firebase/messaging';
import AuthenticatedScreen from './screens/AuthenticatedScreen';
import Geolocation from 'react-native-geolocation-service';
import BackgroundGeolocation from '@mauron85/react-native-background-geolocation';

const credentials = {
  clientId: '1:340237957861:android:e3bf69422ec0320f71f20c',
  appId: '1:340237957861:android:e3bf69422ec0320f71f20c',
  apiKey: 'AIzaSyAn1aKTP3SUOqA6Mgv7NvlE9ygVGBXluEA',
  databaseURL: 'https://besoft-taxi.firebaseio.com',
  storageBucket: 'besoft-taxi.appspot.com',
  messagingSenderId: '340237957861',
  projectId: 'besoft-taxi',
};

@inject('store') @observer
class App extends BaseScreen {
  constructor(props) {
    super(props);

    Promise.all([
      storage.get('token', null),
      storage.get('driver', null),
      storage.get('fcm_token', null),
      storage.get('categories', []),
    ]).then(values => {
      this.store.setDataFromStorage(values);

      //this.checkLastVersion();

      this.getCurrentPosition().then(() => {
        if (this.store.appStore.authenticated) {
          this.appStore.checkAuth();
        }
      });
    });

    if (firebase.apps.length === 0) {
      firebase.initializeApp(credentials);
    }

    this.unsubscribe = messaging().onMessage(async remoteMessage => {
      Alert.alert('A new FCM message arrived!', JSON.stringify(remoteMessage));
    });

    messaging()
      .subscribeToTopic('order')
      .then(() => console.log('Subscribed to topic!'));
  }

  getCurrentPosition = async () => {
    const not_granted_permissions = await this.notGrantedLocationPermissions();
    if (not_granted_permissions === null) {
      Geolocation.getCurrentPosition(
        (position) => {
          this.appStore.setLocation(position.coords.latitude, position.coords.longitude);

          // Geolocation.watchPosition((response) => {
          //   //this.appStore.setLocation(response.coords.latitude, response.coords.longitude);
          // }, (error) => {
          //   console.log(error.code, error.message);
          // }, {
          //   enableHighAccuracy: true,
          //   distanceFilter: 1,
          //   showLocationDialog: true,
          //   forceRequestLocation: true,
          //   interval: 3000,
          //   fastestInterval: 3000,
          // });
        },
        (error) => {
          console.log(error.code, error.message);
        },
        {
          enableHighAccuracy: true,
          distanceFilter: 1,
          forceRequestLocation: true,
        },
      );
    } else {
      this.requestLocationPermissions(not_granted_permissions).then(() => {
      });
    }
  };

  notGrantedLocationPermissions = async () => {
    const coarse = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION);
    const fine = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);

    if (!coarse) {
      return PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION;
    }
    if (!fine) {
      return PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION;
    }

    return null;
  };

  requestLocationPermissions = async (permission) => {
    try {
      const granted = await PermissionsAndroid.request(permission);
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        this.getCurrentPosition();
      } else {
        alert('error');
      }
    } catch (err) {
      console.log(err);
    }
  };

  componentDidMount() {
    BackgroundGeolocation.configure({
      desiredAccuracy: BackgroundGeolocation.HIGH_ACCURACY,
      stationaryRadius: 50,
      distanceFilter: 50,
      notificationTitle: 'GPS иштеп жатат',
      notificationText: 'Такси линияда...',
      debug: false,
      startOnBoot: true,
      stopOnTerminate: false,
      locationProvider: BackgroundGeolocation.ACTIVITY_PROVIDER,
      interval: 5000,
      fastestInterval: 3000,
      activitiesInterval: 5000,
      stopOnStillActivity: false,
    });

    BackgroundGeolocation.on('location', (location) => {
      BackgroundGeolocation.startTask(taskKey => {
        console.log('Location:', location);
        this.appStore.setLocation(location.latitude, location.longitude);
        BackgroundGeolocation.endTask(taskKey);
      });
    });

    BackgroundGeolocation.on('stationary', (stationaryLocation) => {
      console.log('stationaryLocation:', stationaryLocation);
    });

    BackgroundGeolocation.on('error', (error) => {
      console.log('[ERROR] BackgroundGeolocation error:', error);
    });

    BackgroundGeolocation.on('start', () => {
      console.log('[INFO] BackgroundGeolocation service has been started');
    });

    BackgroundGeolocation.on('stop', () => {
      console.log('[INFO] BackgroundGeolocation service has been stopped');
    });

    BackgroundGeolocation.on('authorization', (status) => {
      console.log('[INFO] BackgroundGeolocation authorization status: ' + status);
      if (status !== BackgroundGeolocation.AUTHORIZED) {
        // we need to set delay or otherwise alert may not be shown
        setTimeout(() =>
          Alert.alert('App requires location tracking permission', 'Would you like to open app settings?', [
            {text: 'Yes', onPress: () => BackgroundGeolocation.showAppSettings()},
            {text: 'No', onPress: () => console.log('No Pressed'), style: 'cancel'},
          ]), 1000);
      }
    });

    BackgroundGeolocation.on('background', () => {
      console.log('[INFO] App is in background');
    });

    BackgroundGeolocation.on('foreground', () => {
      console.log('[INFO] App is in foreground');
    });

    BackgroundGeolocation.on('abort_requested', () => {
      console.log('[INFO] Server responded with 285 Updates Not Required');

      // Here we can decide whether we want stop the updates or not.
      // If you've configured the server to return 285, then it means the server does not require further update.
      // So the normal thing to do here would be to `BackgroundGeolocation.stop()`.
      // But you might be counting on it to receive location updates in the UI, so you could just reconfigure and set `url` to null.
    });

    BackgroundGeolocation.on('http_authorization', () => {
      console.log('[INFO] App needs to authorize the http requests');
    });
  }

  componentWillUnmount() {
    this.unsubscribe();
    BackgroundGeolocation.removeAllListeners();
  }

  render() {
    return (
      <>
        <SafeAreaView flex={1}>
          {this.appStore.app_is_ready ? (
            <>
              {this.appStore.authenticated ? (
                <AuthenticatedScreen/>
              ) : (
                <AuthScreen/>
              )}
            </>
          ) : (
            <>
              <StatusBar translucent={true} backgroundColor={'rgba(0, 0, 0, 0)'}/>
              <ImageBackground source={require('./images/bg-taxi.jpg')} style={{
                flex: 1,
                resizeMode: 'cover',
                justifyContent: 'flex-end',
              }}>
                <Card style={{margin: size(4), backgroundColor: 'rgba(255, 255, 255, 0.5)'}} elevation={5}>
                  <Card.Content style={{paddingVertical: size(24)}}>
                    <ActivityIndicator/>
                  </Card.Content>
                </Card>
              </ImageBackground>
            </>
          )}
        </SafeAreaView>
      </>
    );
  }
}

const styles = StyleSheet.create({});

export default App;
