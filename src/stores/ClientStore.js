import {getRoot, types as t} from 'mobx-state-tree';
import Client from '../models/Client';

export default t
  .model('ClientStore', {

    items: t.optional(t.map(Client), {}),

  })
  .actions(self => {

    const setValue = (name, value) => {
      self[name] = value;
    };

    const createOrUpdate = (item) => {
      if (Array.isArray(item)) {
        item.map(v => self.createOrUpdate(v));
        return;
      }

      self.items.set(item.id, {
        ...item,
        last_action: new Date(item.last_action),
        created_at: new Date(item.created_at),
        updated_at: new Date(item.updated_at),
      });
    };

    return {
      setValue,
      createOrUpdate,
    };

  })
  .views(self => ({

    get root() {
      return getRoot(self);
    },

  }));
