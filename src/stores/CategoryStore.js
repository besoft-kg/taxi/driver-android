import {types as t} from 'mobx-state-tree';
import Category from '../models/Category';

export default t
  .model('CategoryStore', {

    items: t.optional(t.map(Category), {}),

  })
  .actions(self => {

    const setValue = (name, value) => {
      self[name] = value;
    };

    const createOrUpdate = (item) => {
      if (Array.isArray(item)) {
        item.map(v => self.createOrUpdate(v));
        return;
      }

      self.items.set(item.id, {
        ...item,
      });
    };

    return {
      setValue,
      createOrUpdate,
    };

  })
  .views(self => ({}));
