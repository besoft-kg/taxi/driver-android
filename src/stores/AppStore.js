import {getRoot, getSnapshot, types as t} from 'mobx-state-tree';
import Driver from '../models/Driver';
import storage from '../utils/storage';
import requester from '../utils/requester';
import float from '../models/float';
import haversine from 'haversine-distance';
import BackgroundGeolocation from '@mauron85/react-native-background-geolocation';
import Alarm from '../models/Alarm';

export default t
  .model('AppStore', {
    user: t.maybeNull(t.reference(Driver)),
    token: t.maybeNull(t.string),
    fcm_token: t.maybeNull(t.string),
    auth_checking: t.optional(t.boolean, false),
    app_is_ready: t.optional(t.boolean, false),
    location_lat: t.maybeNull(float),
    location_lng: t.maybeNull(float),
    locations: t.optional(t.array(t.frozen({lat: float, lng: float})), []),
    alarm: t.maybeNull(Alarm),
    //dark_mode: t.optional(t.boolean, true),
  })
  .actions(self => {

    const setValue = (name, value) => {
      self[name] = value;
    };

    const makeAuth = ({user, token, categories}) => {
      storage.set('token', token);
      self.root.driverStore.createOrUpdate(user);
      self.root.categoryStore.createOrUpdate(categories);
      self.user = user.id;
      storage.set('driver', getSnapshot(self.user));
      storage.set('categories', getSnapshot(self.root.categoryStore.items));
      self.token = token;

      self.checkAuth();

      if (this.appStore.authenticated && this.appStore.user && !this.appStore.user.busy) {
        BackgroundGeolocation.start();
      }
    };

    const clearUser = () => {
      self.user = null;
      self.token = null;
    };

    // const toggleTheme = () => {
    //   self.dark_mode = !self.dark_mode;
    // };

    const checkAuth = () => {
      if (!self.token || self.auth_checking) {
        return;
      }
      self.setValue('auth_checking', true);

      requester.get('/driver/user/basic').then((response) => {
        self.root.driverStore.createOrUpdate(response.payload.user);
        self.root.categoryStore.createOrUpdate(response.payload.categories);
        self.root.orderStore.createOrUpdate(response.payload.orders.pending);
        if (response.payload.orders.active) self.root.orderStore.createOrUpdate(response.payload.orders.active);
        if (response.payload.taximeter) self.root.taximeterStore.createOrUpdate(response.payload.taximeter);

        self.setValue('user', response.payload.user.id);
        storage.set('driver', getSnapshot(self.user));
        storage.set('categories', getSnapshot(self.root.categoryStore.items));
        BackgroundGeolocation.start();
      }).catch(e => {
        console.log(e);
        self.clearUser();
      }).finally(() => {
        self.setValue('auth_checking', false);
      });
    };

    const setLocation = (lat, lng) => {
      if (self.authenticated) {
        self.user.setLocation(lat, lng);
        if (self.root.taximeterStore.active_item) {
          self.root.taximeterStore.active_item.addLocation(lat, lng);
        }
      }

      self.location_lat = lat;
      self.location_lng = lng;

      self.locations.push({
        lat, lng,
      });
    };

    const signOut = async () => {
      self.user = null;
      self.token = null;
      await storage.remove('driver');
      await storage.remove('token');
    };

    const setBusy = (busy) => {
      if (self.authenticated && self.user) {
        self.user.setValue('busy', busy);
      }
    };

    return {
      signOut,
      setValue,
      makeAuth,
      checkAuth,
      clearUser,
      setLocation,
      setBusy,
      // toggleTheme,
    };

  })
  .views(self => ({

    get root() {
      return getRoot(self);
    },

    get distance() {
      let result = 0;
      self.locations.map((v, i) => {
        console.log(v, i);
        if (i > 0) {
          const prev_location = self.locations[i - 1];
          result += haversine({
            lat: +prev_location.lat,
            lng: +prev_location.lng,
          }, {
            lat: +v.lat,
            lng: +v.lng,
          });
          console.log(result);
        }
      });
      return result.toFixed(2);
    },

    get authenticated() {
      return self.user !== null;
    },

  }));
