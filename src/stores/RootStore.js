import {types as t} from 'mobx-state-tree';
import AppStore from './AppStore';
import DriverStore from "./DriverStore";
import CategoryStore from './CategoryStore';
import OrderStore from './OrderStore';
import TaximeterStore from './TaximeterStore';
import AlarmStore from './AlarmStore';
import ClientStore from './ClientStore';

export default t
  .model('RootStore', {

    appStore: AppStore,
    driverStore: DriverStore,
    categoryStore: CategoryStore,
    orderStore: OrderStore,
    taximeterStore: TaximeterStore,
    alarmStore: AlarmStore,
    clientStore: ClientStore,

  })
  .actions(self => {

    const setDataFromStorage = (values) => {
      if (values[0]) self.appStore.token = values[0];
      if (values[1]) {
        self.driverStore.createOrUpdate(values[1]);
        self.appStore.user = values[1].id;
      }
      if (values[2]) self.appStore.fcm_token = values[2];
      self.categoryStore.createOrUpdate(Object.values(values[3]));
      self.appStore.app_is_ready = true;
    };

    return {
      setDataFromStorage,
    };

  })
  .create({

    appStore: AppStore.create(),
    driverStore: DriverStore.create(),
    categoryStore: CategoryStore.create(),
    orderStore: OrderStore.create(),
    taximeterStore: TaximeterStore.create(),
    alarmStore: AlarmStore.create(),
    clientStore: ClientStore.create(),

  });
