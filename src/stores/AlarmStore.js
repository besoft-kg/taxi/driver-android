import {getRoot, types as t} from 'mobx-state-tree';
import {values} from 'mobx';
import Alarm from '../models/Alarm';

export default t
  .model('AlarmStore', {

    items: t.optional(t.map(Alarm), {}),

  })
  .actions(self => {

    const setValue = (name, value) => {
      self[name] = value;
    };

    const remove = (id) => {
      self.items.delete(id);
    };

    const createOrUpdate = (item) => {
      if (Array.isArray(item)) {
        item.map(v => self.createOrUpdate(v));
        return;
      }

      if (item.driver) self.root.driverStore.createOrUpdate(item.driver);

      self.items.set(item.id, {
        ...item,
        created_at: new Date(item.created_at),
        driver: item.driver ? item.driver.id : null,
      });
    };

    return {
      setValue,
      createOrUpdate,
      remove,
    };

  })
  .views(self => ({

    get root() {
      return getRoot(self);
    },

    get active_item() {
      return values(self.items).find(v => v.driver_id !== self.root.appStore.user.id && !v._cancelled);
    },

  }));
