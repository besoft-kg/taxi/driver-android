import React from 'react';
import BaseComponent from "../components/BaseComponent";
import {Card} from "react-native-paper";
import {Text, View} from "react-native";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {APP_COLOR, size} from "../utils";


class MeterComponent extends BaseComponent {

  render() {
    const item = this.props.item
    return (
      <>
        <Card alignContent={'space-between'} style={{backgroundColor: '#1A5276'}}>
          <Card.Content>
            <View alignItems={'center'}>
              <Icon/>
              <Text style={{fontSize: 54, color: '#FDFEFE'}}>{item.taximeter.amount_to_pay} сом</Text>
            </View>
            <View alignItems={'center'}>
              <Text style={{fontSize: 34, color: '#FDFEFE'}}>{item.taximeter.ui_distance}</Text>
            </View>
          </Card.Content>
        </Card>
        <Card style={{backgroundColor: '#1C2833', padding: size(1)}}>
          <Card.Content>
            <View flexDirection={'row'} justifyContent={'space-between'} alignItems={'center'}>
              <Text style={{fontSize: 24, color: '#E67E22'}}>Посадка</Text>
              <Text style={{fontSize: 24, color: '#EB984E'}}>{item.category.landing_price} сом</Text>
            </View>
            <View flexDirection={'row'} justifyContent={'space-between'} alignItems={'center'}>
              <Text style={{fontSize: 24, color: '#E67E22'}}>Дистанция</Text>
              <Text style={{fontSize: 24, color: '#EB984E'}}>{item.taximeter.ui_distance}</Text>
            </View>
            <View flexDirection={'row'} justifyContent={'space-between'} alignContent={'space-between'}>
              <Text style={{fontSize: 24, color: '#E67E22'}}>Эсеби</Text>
              <Text style={{fontSize: 24, color: '#EB984E'
              }}>{item.taximeter.amount_to_pay - item.category.landing_price} сом</Text>
            </View>
            <View flexDirection={'row'} justifyContent={'space-between'} alignItems={'center'}>
              <Text style={{fontSize: 24, color: '#E67E22'}}>Күтүү</Text>
              <Text style={{fontSize: 24, color: '#EB984E'}}>0.00</Text>
            </View>
            <View   alignItems={'center'}>
              <Text style={{fontSize: 34, color: APP_COLOR}}>{item.taximeter.amount_to_pay} сом</Text>
            </View>
          </Card.Content>
        </Card>
      </>
    )
  }
}

export default MeterComponent;
