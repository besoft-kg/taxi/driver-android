import React from 'react'
import BaseScreen from "../screens/BaseScreen";
import {ActivityIndicator, Card} from 'react-native-paper';
import {APP_COLOR, size} from "../utils";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import {Text, TouchableOpacity, Image, Alert} from "react-native";
import ImagePicker from 'react-native-image-picker';
import {observer} from "mobx-react";

@observer
class ImageSelector extends BaseScreen {
  selectImage = () => {
    const options = {
      title: 'Сүрот тандоо',
      takePhotoButtonTitle: 'Сүроткө тартуу',
      chooseFromLibraryButtonTitle: 'Галереядан сүрөт тандоо',
      cancelButtonTitle: 'Артка',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log(response);

      if (!response.didCancel && !response.error) {
        this.props.onSelect(response);
      } else if (response.error){
         Alert.alert('Катачылык', "Камерага же Галереяга доступ жок!");
      }
    });
  };

  content = () => {
    if (this.props.loading) {
      return (
        <ActivityIndicator/>
      );
    }

    if (!this.props.value) {
      return (
        <>
          <Icon name="camera" size={30} color={APP_COLOR}/>
          <Text>{this.props.label}</Text>
        </>
      )
    } else {
      return (
        <>
          <Image style={{height: 120, width: 200, marginBottom: size(4)}} source={{uri: this.props.value.url.original}}/>
          <Text>{this.props.label}</Text>
        </>
      );
    }
  };

  render() {
    return (
      <>
        <TouchableOpacity activeOpacity={.8} onPress={() => this.selectImage()}>
          <Card style={{margin: size(4), backgroundColor: 'rgba(255, 255, 255, 0.8)', height: 170}} elevation={5}>
            <Card.Content flex={1} justifyContent={'center'} alignItems={'center'}>
              {this.content()}
            </Card.Content>
          </Card>
        </TouchableOpacity>
      </>
    )
  }
}

export default ImageSelector;
