import React from 'react';
import {Button as PaperButton, useTheme} from "react-native-paper";

export default function Button(props) {
  const theme = useTheme();
  const {style: _style, labelStyle, variant, ..._props} = props;

  // const {dark} = theme;

  const styles = {
    dark: {
      primary: {
        borderRadius: 20,

        _labelStyle: {
          color: theme.colors.text,
          fontSize: 22,
        },

        _mode: 'contained',
      },
      secondary: {
        borderRadius: 20,
        backgroundColor: theme.colors.surface,

        _labelStyle: {
          color: theme.colors.text,
          fontSize: 22,
        },

        _mode: 'outlined',
      },
      danger: {
        borderRadius: 20,
        backgroundColor: theme.colors.error,

        _labelStyle: {
          color: theme.colors.text,
          fontSize: 22,
        },

        _mode: 'outlined',
      }
    },
    // light: {
    //   primary: {
    //     borderRadius: 20,
    //
    //     _labelStyle: {
    //       color: theme.colors.text,
    //       fontSize: 22,
    //     },
    //
    //     _mode: 'contained',
    //   }
    // }
  };

  // const style = styles[dark ? 'dark' : 'light'][variant];
  const style = styles['dark'][variant];

  return (
    <PaperButton
      {..._props}
      labelStyle={[labelStyle, style._labelStyle]}
      mode={style._mode}
      style={[style, _style]}
    />
  );
}
