import axios from 'axios';
import {GOOGLE_CLOUD_API_KEY} from './index';

export const maps = {
  distancematrix: async (origins, destinations) => {
    try {
      const response = await axios.get(
        'https://maps.googleapis.com/maps/api/distancematrix/json',
        {
          params: {
            key: GOOGLE_CLOUD_API_KEY,
            language: 'ky-KG',
            origins, destinations,
          },
        },
      );
      return response.data;
    } catch (e) {
      console.log(e);
    }
  },
};
