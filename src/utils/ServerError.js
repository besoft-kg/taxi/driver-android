class ServerError extends Error {
  message = 'Произошла неизвестная ошибка!';
  title = 'Катачылык!';
  result = -1;
  payload = '';
  status = 'unknown_error';
  retry_function = null;
  error = null;

  constructor(response = null, retry_function = null) {
    super(response.data.status);
    if (response) this.setResponse(response);
    this.setRetryFunction(retry_function);
  }

  setError(error) {
    if (error.response) {
      this.setResponse(error.response);
    } else {
      this.message = error.message === 'Network Error' ? 'Сетевая ошибка!' : error.message;
    }
    this.error = error;
    return this;
  }

  setRetryFunction = (func) => {
    this.retry_function = func;
    return this;
  };

  setMessage = (message) => {
    this.message = message;
    return this;
  };

  setTitle = (string) => {
    this.title = string;
    return this;
  };

  toSnapshot = () => {
    return {
      message: this.message,
      title: this.title,
      status: this.status,
      payload: this.payload,
      result: this.result,
      //retry_function: this.retry_function,
      error: this.error,
    };
  };

  fromSnapshot = (snapshot) => {
    this.message = snapshot.message;
    this.title = snapshot.title;
    this.status = snapshot.status;
    this.payload = snapshot.payload;
    this.result = snapshot.result;
    //this.retry_function = snapshot.retry_function;
    this.error = snapshot.error;
  };

  setResponse = ({data}) => {
    this.payload = data.payload;
    this.result = data.result;
    this.status = data.status;

    if (data.result < 0) {
      switch (data.status) {
        case 'access_denied':
          this.message = 'Доступ запрещен!';
          break;

        case 'invalid_params':
          this.message = 'Неверные параметры!';
          break;
      }
    }

    if ('message' in this.payload && typeof this.payload.message === 'string' && this.payload.message) {
      this.message = this.payload.message;
    }

    return this;
  };
}

export default ServerError;
