import {Alert, PixelRatio} from 'react-native';
import FormData from 'form-data';
import requester from './requester';
import RootStore from '../stores/RootStore';

export const APP_NAME = 'Besoft Такси';
export const APP_COLOR = '#01c5c4';

export const DEBUG_MODE = process.env.NODE_ENV !== "production";
export const API_VERSION = 1;
export const APP_VERSION_CODE = 1;
export const APP_VERSION_NAME = '0.1';
export const API_URL = 'https://api.taxi.besoft.kg';

export const GOOGLE_CLOUD_API_KEY = 'AIzaSyB9TAxW6_Zf-Cfy1Z6Dk4tafFy8Y2LL7Kc';

export const size = (dp) => PixelRatio.getPixelSizeForLayoutSize(dp);

export const isFile = (v) => typeof v === 'object' && Object.keys(v).length === 3 && 'type' in v && 'uri' in v && 'name' in v;

export const buildFormData = (formData, data, parentKey) => {
  if (data && typeof data === 'object' && !(data instanceof Date) && !isFile(data)) {
    Object.keys(data).forEach(key => {
      buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
    });
  } else {
    const value = data == null ? '' : data;
    formData.append(parentKey, value);
  }
};

export const jsonToFormData = (data) => {
  const formData = new FormData();
  buildFormData(formData, data);
  return formData;
};

export const alarmAnnouncement  = ({onBusy, onFree}) => {
  Alert.alert('Тревога', 'Тревога жарыялоого оюңуз бекемби?', [
    {
      text: 'Жок',
      style: 'cancel',
    },
    {
      text: 'Ооба', onPress: async () => {
        onBusy();
        requester.post('/driver/user/alarm', {
          address_lat: RootStore.appStore.location_lat,
          address_lng: RootStore.appStore.location_lng,
        }).then(response => {
          switch (response.status) {
            case 'success':
              RootStore.appStore.setValue('alarm', {
                ...response.payload,
                driver: null,
                created_at: new Date(response.payload.created_at),
              });
              break;
            case 'limit_exceeded':
              Alert.alert('Лимит жеткен!', '1 суткада 3 жолу гана тревога жарыялаганга болот.');
              break;
          }
        }).catch(e => console.log(e)).finally(() => {
          onFree();
        });
      },
    }
  ], {
    cancelable: true,
  });
};
