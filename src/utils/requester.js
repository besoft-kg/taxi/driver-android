import axios from 'axios';
import storage from './storage';
import ServerError from './ServerError';
import ConnectionError from './ConnectionError';
import {API_URL, API_VERSION, jsonToFormData} from './index';
import {Alert} from 'react-native';
import RootStore from "../stores/RootStore";

const request = async (method, url, data = {}) => {
  try {
    const headers = {
      'Accept': 'application/json',
      'Content-Type': method === 'post' ? 'application/x-www-form-urlencoded' : 'application/json'
    };
    const token = await storage.get('token');
    if (token) {
      headers['Authorization'] = `Bearer ${token}`;
    }

    const response = await axios.request({
      url: `${API_URL}/api/v${API_VERSION}${url}`,
      method,
      timeout: 5000,
      responseType: 'json',
      responseEncoding: 'utf8',
      [method === 'post' ? 'data' : 'params']: method === 'post' ? jsonToFormData(data) : data,
      headers,
    });

    if (response.data.result < 0) {
      if (response.data.status === "token_is_invalid") {
        RootStore.appStore.signOut().then(() => {});
      }

      throw new ServerError(response);
    }

    return response;
  } catch (e) {
    if (e instanceof ServerError) {
      console.log(e.toSnapshot());
      throw e;
    } else {
      if (e.response) {
        if (e.response.data.status === 'unauthorized') {
          RootStore.appStore.signOut().then(() => {});
        }
        throw new ConnectionError(e.response);
      } else if (e.request) {
        console.log(e.request);
        //throw new ConnectionError(e.request);
      } else {
        throw e;
      }
    }
  }
};

const failure = (e) => {
  Alert.alert(e.title, e.message);
};

const post = async (url, data = {}) => {
  try {
    const response = await request('post', url, data);
    return response.data;
  } catch (e) {
    failure(e);
    throw e;
  }
};

const get = async (url, data) => {
  try {
    const response = await request('get', url, data);
    return response.data;
  } catch (e) {
    failure(e);
    throw e;
  }
};

export default {
  post,
  get,
  delete: async (url, data) => {
    try {
      const response = await request('delete', url, data);
      return response.data;
    } catch (e) {
      failure(e);
      throw e;
    }
  },
};
