import React from 'react';
import {Provider} from 'mobx-react';
import {Provider as PaperProvider, DarkTheme} from 'react-native-paper';
import App from './App';
import RootStore from './stores/RootStore';
import Toast from 'react-native-toast-message';
import {Observer} from 'mobx-react';

const themes = {
  // light: {
  //   ...DefaultTheme,
  //   dark: false,
  //   colors: {
  //     ...DefaultTheme.colors,
  //     primary: '#80e27e',
  //     accent: '#82e0aa',
  //     placeholder: 'green',
  //     background: '#4c8c4a',
  //     surface: '#d7ffd9',
  //     text: '#212F3C',
  //     error: '#B00020',
  //   },
  // },
  dark: {
    ...DarkTheme,
    dark: true,
    colors: {
      ...DarkTheme.colors,
      primary: '#087f23',
      accent: '#5d99c6',
      placeholder: 'green',
      background: '#121212',
      surface: '#092219',
      text: '#fff',
      error: '#B00020',
    },
  },
};

const Root = () => {
  // const theme = themes[RootStore.appStore.dark_mode ? 'dark' : 'light'];
  const theme = themes['dark'];

  return (
    <>
      <Toast ref={(ref) => Toast.setRef(ref)}/>
      <Provider store={RootStore}>
        <PaperProvider theme={theme}>
          <App/>
        </PaperProvider>
      </Provider>
    </>
  );
};

export default Root;
