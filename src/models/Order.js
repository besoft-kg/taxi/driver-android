import {getRoot, types as t} from 'mobx-state-tree';
import Category from './Category';
import Admin from './Admin';
import Client from './Client';
import float from './float';
import {Text} from 'react-native-paper';
import Driver from './Driver';
import haversine from 'haversine-distance';
import React from 'react';
import {size} from '../utils';
import Taximeter from './Taximeter';

export default t
  .model('order', {
    id: t.identifierNumber,
    publisher_id: t.maybeNull(t.integer),
    publisher: t.maybeNull(t.reference(Admin)),
    client_id: t.maybeNull(t.integer),
    client: t.maybeNull(t.reference(Client)),
    category_id: t.integer,
    category: t.maybeNull(t.reference(Category)),
    phone_number: t.maybeNull(t.string),
    status: t.enumeration([
      'pending',
      'fake_call',
      'fake_call_checking',
      'cancelled_by_client',
      'cancelled_by_admin',
      'going_to_client',
      'in_place',
      'going_with_client',
      'completed',
    ]),
    note: t.maybeNull(t.string),
    history: t.maybeNull(t.array(t.frozen())),
    payload: t.maybeNull(t.frozen()),
    address: t.maybeNull(t.string),
    address_lat: t.maybeNull(float),
    address_lng: t.maybeNull(float),
    created_at: t.Date,
    updated_at: t.Date,
    executor: t.maybeNull(t.reference(Driver)),
    executor_id: t.maybeNull(t.integer),
    // duration: t.maybeNull(t.integer),
    // duration_text: t.maybeNull(t.string),

    is_new: t.optional(t.boolean, true),
    taximeter: t.maybeNull(t.reference(Taximeter)),
    fully_completed: t.optional(t.boolean, false),
  }).views(self => ({

    get can_be_accepted() {
      const distance = Math.ceil((self.distance / 1000));
      const difference = self.difference_time;

      if (difference < 10) {
        return distance <= 2;
      } else if (difference < 20) {
        return distance <= 5;
      } else {
        return true;
      }
    },

    get root() {
      return getRoot(self);
    },

    get cancelled() {
      return self.status.startsWith('cancelled_by_');
    },

    get difference_time() {
      return +((new Date().getTime() / 1000).toFixed()) - +((self.created_at.getTime() / 1000).toFixed());
    },

    get distance() {
      return haversine({
        lat: self.address_lat,
        lng: self.address_lng,
      }, {
        lat: self.root.appStore.location_lat,
        lng: self.root.appStore.location_lng,
      });
    },

    get distance_text() {
      return `${(self.distance / 1000).toFixed(2)} км`;
    },

    get ui_category() {
      return (
        <Text style={{
          backgroundColor: '#eee',
          borderWidth: 1,
          borderColor: '#eee',
          borderRadius: 10,
          padding: size(1),
          color: 'black',
          fontSize: 14,
          fontWeight: 'bold',
        }}>{self.category.title}</Text>
      );
    },

    get last_history() {
      if (!self.history) return null;
      return self.history[self.history.length - 1];
    },

  })).actions(self => {

    const setValue = (name, value) => {
      self[name] = value;
    };

    const markAsOld = () => {
      self.is_new = false;
    };

    const afterCreate = () => {
      setTimeout(() => {
        self.setValue('is_new', false);
      }, 1000);

      // maps.distancematrix(
      //   `${self.root.appStore.user.location_lat},${self.root.appStore.user.location_lng}`,
      //   `${self.address_lat},${self.address_lng}`
      // ).then(response => {
      //   console.log(response);
      //
      //   switch (response.rows[0].elements[0].status) {
      //     case 'ZERO_RESULTS':
      //
      //       break;
      //     case 'OK':
      //       self.setValue('distance', response.rows[0].elements[0].distance.value);
      //       self.setValue('distance_text', response.rows[0].elements[0].distance.text);
      //       self.setValue('duration', response.rows[0].elements[0].duration.value);
      //       self.setValue('duration_text', response.rows[0].elements[0].duration.text);
      //       break;
      //   }
      // });
    };

    return {
      setValue,
      afterCreate,
      markAsOld,
    };

  });
