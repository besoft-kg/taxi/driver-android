import {types as t} from 'mobx-state-tree';
import React from 'react';
import Driver from './Driver';
import float from './float';

export default t
  .model('alarm', {
    id: t.identifierNumber,
    driver_id: t.integer,
    driver: t.maybeNull(t.reference(Driver)),
    address_lat: t.maybeNull(float),
    address_lng: t.maybeNull(float),
    created_at: t.Date,

    _cancelled: t.optional(t.boolean, false),
  }).views(self => ({})).actions(self => {

    const setValue = (name, value) => {
      self[name] = value;
    };

    return {
      setValue,
    };

  });
