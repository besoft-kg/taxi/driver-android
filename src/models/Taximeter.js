import {types as t} from 'mobx-state-tree';
import haversine from 'haversine-distance';
import float from './float';
import requester from '../utils/requester';

export default t
  .model('taximeter', {
    id: t.identifierNumber,
    order_id: t.maybeNull(t.integer),
    driver_id: t.integer,
    started_at: t.Date,
    ended_at: t.maybeNull(t.Date),
    amount_to_pay: t.integer,
    duration_in_seconds: t.optional(t.integer, 0),
    waiting_time_in_seconds: t.optional(t.integer, 0),
    distance_in_m: t.optional(t.integer, 0),
    amount_info: t.frozen(),
    locations: t.array(t.frozen({
      lat: float,
      lng: float,
    })),
    created_at: t.Date,
    updated_at: t.Date,

    _waiting_interval_id: t.maybeNull(t.integer),
    fully_completed: t.optional(t.boolean, false),
  }).views(self => ({

    get distance() {
      let result = 0;
      self.locations.map((v, i) => {
        if (i > 0 && v.lat && v.lng) {
          const prev_location = self.locations[i - 1];
          if (prev_location.lat && prev_location.lng) {
            result += haversine({
              lat: +prev_location.lat,
              lng: +prev_location.lng,
            }, {
              lat: +v.lat,
              lng: +v.lng,
            });
          }
        }
      });
      return result;
    },

    get ui_distance() {
      if (self.distance.toFixed() < 1) {
        return '0.0 км';
      }
      return `${(self.distance / 1000).toFixed(1)} км`;
    },

    get last_location() {
      if (self.locations.length === 0) {
        return null;
      }
      return self.locations[self.locations.length - 1];
    },

    get ui_waiting_time() {
      const minutes = Math.floor(self.waiting_time_in_seconds / 60) > 9 ? Math.floor(self.waiting_time_in_seconds / 60) : `0${Math.floor(self.waiting_time_in_seconds / 60)}`;
      const seconds = self.waiting_time_in_seconds % 60 > 9 ? self.waiting_time_in_seconds % 60 : `0${self.waiting_time_in_seconds % 60}`;
      return `${minutes}:${seconds}`;
    },

    get waiting_time_price() {
      return Math.floor(self.waiting_time_in_seconds / 60) * self.amount_info.price_for_waiting_minute;
    },

    get ui_waiting_time_price() {
      return `${self.waiting_time_price} с`;
    },

  })).actions(self => {

    const startWaiting = () => {
      if (self._waiting_interval_id) {
        return;
      }
      self._waiting_interval_id = setInterval(() => {
        self.setValue('waiting_time_in_seconds', self.waiting_time_in_seconds + 1);
        self.setValue('amount_to_pay', +self.amount_info.landing_price + +(self.distance * (+self.amount_info.price_for_km / 1000)).toFixed() + self.waiting_time_price);
      }, 1000);
    };

    const setValue = (name, value) => {
      self[name] = value;
    };

    const stopWaiting = () => {
      clearInterval(self._waiting_interval_id);
      self._waiting_interval_id = null;
    };

    const addLocation = (lat, lng) => {
      const {last_location} = self;

      if (!last_location || haversine({
        lat: last_location.lat,
        lng: last_location.lng,
      }, {
        lat, lng,
      }) >= 10) {
        self.locations.push({
          lat, lng,
        });

        requester.post('/driver/taximeter/location', {
          latitude: lat,
          longitude: lng,
          id: self.id,
        }).then((response) => {}).catch(e => {
          console.log(e);
        });

        if (self._waiting_interval_id) {
          self.stopWaiting();
        }
      }

      if (self.distance >= 1) {
        self.amount_to_pay = +self.amount_info.landing_price + +(self.distance * (+self.amount_info.price_for_km / 1000)).toFixed() + self.waiting_time_price;
      }
    };

    return {
      setValue,
      addLocation,
      startWaiting,
      stopWaiting,
    };

  });
