import {types as t} from "mobx-state-tree";
import Image from "./Image";
import Category from "./Category";
import Admin from "./Admin";
import float from './float';

export default t
  .model('driver', {
    id: t.identifierNumber,
    phone_number: t.string,
    full_name: t.maybeNull(t.string),
    gender: t.maybeNull(t.enumeration(['male', 'female'])),
    contacts: t.maybeNull(t.array(t.frozen())),
    picture_id: t.maybeNull(t.integer),
    picture: t.maybeNull(Image),
    balance: t.integer,
    categories: t.maybeNull(t.array(t.reference(Category))),
    confirmed_by_id: t.maybeNull(t.integer),
    confirmed_by: t.maybeNull(t.reference(Admin)),
    confirmed_at: t.maybeNull(t.Date),
    vehicle_picture_id: t.maybeNull(t.integer),
    vehicle_picture: t.maybeNull(Image),
    state_reg_plate: t.maybeNull(t.string),
    datasheet_picture_id: t.maybeNull(t.integer),
    datasheet_picture: t.maybeNull(Image),
    drivers_license_picture_id: t.maybeNull(t.integer),
    drivers_license_picture: t.maybeNull(Image),
    drivers_license_expiration_date: t.maybeNull(t.Date),
    last_action: t.Date,
    busy: t.boolean,
    location_lat: t.maybeNull(float),
    location_lng: t.maybeNull(float),
    location_last_refresh: t.maybeNull(t.Date),
    created_at: t.maybeNull(t.Date),
    updated_at: t.maybeNull(t.Date),
  }).actions(self => {

      const setValue = (name, value) => {
          self[name] = value;
      };

      const setLocation = (lat, lng) => {
          self.location_lat = lat;
          self.location_lng = lng;
      };

      return {
          setLocation,
          setValue,
      };

  }).views(self => ({

      get is_confirmed() {
          return self.confirmed_by_id && self.confirmed_at;
      },

      get first_name() {
          const arr = self.full_name.split(' ');
          if (arr.length < 1) return null;
          return arr[0];
      },

      get last_name() {
          const arr = self.full_name.split(' ');
          if (arr.length < 2) return null;
          return arr[1];
      },

  }));
