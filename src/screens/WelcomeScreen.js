import React from 'react';
import BaseScreen from './BaseScreen';
import {Text, StatusBar, ImageBackground, View, Linking} from 'react-native';
import {inject, observer} from 'mobx-react';
import ImageSelector from '../ui/ImageSelector';
import {APP_COLOR, size} from '../utils';
import {action, computed, observable} from 'mobx';
import requester from '../utils/requester';
import ViewPager from '@react-native-community/viewpager';
import Button from "../ui/Button";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Card, Title} from "react-native-paper";

@inject('store') @observer
class WelcomeScreen extends BaseScreen {

  view_pager_ref = React.createRef();

  call = () => {
    Linking.openURL(`tel:+${996777171171}`);
  };

  @observable loading = {
    datasheet_picture: false,
    drivers_license_picture: false,
    vehicle_picture: false,
    picture: false,
  };

  @observable current_page = 0;

  @computed get active_page() {
    if (!this.appStore.user.datasheet_picture) return 0;
    else if (!this.appStore.user.drivers_license_picture) return 1;
    else if (!this.appStore.user.vehicle_picture) return 2;
    else if (!this.appStore.user.picture) return 3;
    return 4;
  }

  @computed get can_next_page() {
    return this.current_page < 4 && this.active_page > this.current_page;
  }

  @computed get can_prev_page() {
    return this.current_page > 0;
  }

  @action upload = (type, file) => {
    if (this.loading[type]) return;
    this.loading[type] = true;
    requester.post('/driver/user/document', {
      type,
      image: {
        name: file.fileName,
        type: file.type,
        uri: file.uri,
      },
    }).then((response) => {
      if (response.status === 'success') {
        this.driverStore.createOrUpdate(response.payload);
        this.nextPage();
      }
    }).catch(e => {
      this.setValue(type, null);
      console.log(e);
    }).finally(() => {
      this.loading[type] = false;
    });
  };

  nextPage = () => {
    this.setValue('current_page', this.current_page + 1);
    this.view_pager_ref.setPage(this.current_page);
  };

  prevPage = () => {
    this.setValue('current_page', this.current_page - 1);
    this.view_pager_ref.setPage(this.current_page);
  };

  render() {
    return (
      <>
        <StatusBar translucent={true} backgroundColor={'rgba(0, 0, 0, 0)'}/>
        <ImageBackground source={require('../images/bg-taxi.jpg')} style={{
          flex: 1,
          resizeMode: 'cover',
          justifyContent: 'flex-start',
        }}>
          {this.can_next_page ?
            (<Text style={{
                backgroundColor: APP_COLOR, paddingTop: StatusBar.currentHeight + 12,
                textAlign: 'center', padding: 15,
                fontSize: 24,
              }}>Катталуудан өтүңүз</Text>
            ) : (<Text style={{
              backgroundColor: APP_COLOR, paddingTop: StatusBar.currentHeight + 12,
              textAlign: 'center', padding: 15,
              fontSize: 24,
            }}>Күтүңүз...</Text>)
          }
          <ViewPager
            onPageSelected={({nativeEvent}) => this.setValue('current_page', nativeEvent.position)}
            ref={ref => this.view_pager_ref = ref}
            flex={1}
            initialPage={this.active_page}
            keyboardDismissMode={'none'}
          >
            <View key="1" justifyContent={'flex-end'}>
              <ImageSelector loading={this.loading.datasheet_picture} value={this.appStore.user.datasheet_picture}
                             onSelect={(file) => this.upload('datasheet_picture', file)} label={'Техникалык паспорт'}/>
            </View>
            <View key="2" justifyContent={'flex-end'}>
              <ImageSelector loading={this.loading.drivers_license_picture}
                             value={this.appStore.user.drivers_license_picture}
                             onSelect={(file) => this.upload('drivers_license_picture', file)}
                             label={'Айдоочулук күбөлүк'}/>
            </View>
            <View key="3" justifyContent={'flex-end'}>
              <ImageSelector loading={this.loading.vehicle_picture} value={this.appStore.user.vehicle_picture}
                             onSelect={(file) => this.upload('vehicle_picture', file)} label={'Автоунаа сүрөтү'}/>
            </View>
            <View key="4" justifyContent={'flex-end'}>
              <ImageSelector loading={this.loading.picture} value={this.appStore.user.picture}
                             onSelect={(file) => this.upload('picture', file)} label={'Жеке сүрөт'}/>
            </View>
            <View key="5" justifyContent={'center'}>
              <Card style={{margin: size(4), backgroundColor: 'rgba(255, 255, 255, 0.8)', height: 245}}>
                <Card.Content>
                  <Text  style={{textAlign: 'center'}}>
                    <Icon size={size(30)} flex={1} style={{color: APP_COLOR}} name={'alarm'}/>
                  </Text>
                  <Title flex={1} style={{color: '#121212', textAlign: 'center'}}>Card title</Title>
                  <Text style={{marginTop: size(4), textAlign: 'center'}}>hjkghjvcsdvsdfsdvsd</Text>
                </Card.Content>
              </Card>
            </View>
          </ViewPager>

          {this.can_next_page ? (
            <Button onPress={() => this.nextPage()} disabled={!this.can_next_page} style={{margin: size(4)}}
                    variant={'primary'}>Кийинки</Button>
          ) : (
            <Button onPress={() => this.call()} style={{margin: size(4)}} variant={'primary'}>Биз м/н
              байланушуу</Button>
          )}
          <Button onPress={() => this.prevPage()} disabled={!this.can_prev_page} style={{margin: size(4)}}
                  variant={'secondary'}>Мурунку</Button>
        </ImageBackground>
      </>
    );
  }
}

export default WelcomeScreen;
