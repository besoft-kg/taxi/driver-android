import React from 'react';
import {inject, observer} from 'mobx-react';
import WelcomeScreen from './WelcomeScreen';
import MainScreen from './MainScreen';
import BaseScreen from './BaseScreen';
import Pusher from 'pusher-js/react-native';
import {API_URL, DEBUG_MODE} from '../utils';

@inject('store') @observer
class AuthenticatedScreen extends BaseScreen {
  pusher = null;

  constructor(props) {
    super(props);

    Pusher.logToConsole = DEBUG_MODE;

    this.pusher = new Pusher('8c0e90a6896b7c598920', {
      cluster: 'eu',
      authEndpoint: `${API_URL}/broadcasting/auth`,
      auth: {
        headers: {
          'Authorization': `Bearer ${this.appStore.token}`,
        },
      },
    });

    const driver_channel = this.pusher.subscribe(`private-driver.${this.appStore.user.id}`);

    driver_channel.bind('on-update', (data) => {
      this.driverStore.createOrUpdate(data.item);
    });

    // driver_channel.bind('pusher:subscription_succeeded', (data) => {
    //   this.setValue('network_status', 'connected');
    // });
  }

  render() {
    return (
      <>
        {!this.appStore.user.is_confirmed ? <WelcomeScreen/> : <MainScreen pusher={this.pusher}/>}
      </>
    );
  }
}

export default AuthenticatedScreen;
