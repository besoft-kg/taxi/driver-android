import React from 'react';
import {Card, TextInput} from 'react-native-paper';
import {StatusBar, ImageBackground} from 'react-native';
import {APP_VERSION_CODE, size} from '../utils';
import {action, computed, observable} from 'mobx';
import BaseScreen from './BaseScreen';
import PhoneNumber from '../utils/PhoneNumber';
import {inject, observer} from 'mobx-react';
import requester from '../utils/requester';
import Button from "../ui/Button";

@inject('store') @observer
class AuthScreen extends BaseScreen {
  @observable phone_number = '';
  @observable loading = false;
  @observable checked = false;
  @observable status = '';
  @observable verification_code = '';

  constructor(props) {
    super(props);

    this.phone_object = new PhoneNumber();
  }

  @action setPhoneNumber = (v) => {
    this.phone_number = v;
    try {
      this.phone_object.parseFromString(v);
    } catch (e) {

    }
  };

  @action sendCode = () => {
    if (this.loading) {
      return;
    }
    this.checked = true;
    if (!this.phone_is_valid) {
      return;
    }

    this.loading = true;
    requester.post('/driver/auth/phone', {
      phone_number: this.phone_object.getE164Format().slice(1),
    }).then((response) => {
      console.log(response);
      this.status = response.status;
    }).catch(e => {
      console.log(e);
    }).finally(() => {
      this.loading = false;
    });
  };

  @action checkCode = () => {
    if (this.loading) {
      return;
    } else if (this.verification_code.length === 0) {
      alert('Тастыктоо кодун жазыңыз!');
      return;
    } else if (this.verification_code.length !== 4) {
      alert('Тастыктоо коду туура эмес!');
      return;
    }

    this.loading = true;

    if (this.status.startsWith('not_exists_')) {

      requester.post('/driver/auth/phone/register', {
        phone_number: this.phone_object.getE164Format().slice(1),
        verification_code: this.verification_code,
        fcm_token: '',
        platform: 'android',
        version_code: APP_VERSION_CODE,
      }).then((response) => {

        console.log(response);

        switch (response.status) {
          case 'success':
            this.appStore.makeAuth(response.payload);
            break;
        }

      }).catch(e => {
        console.log(e);
      }).finally(() => {
        this.loading = false;
      });

    } else if (this.status.startsWith('exists_')) {

      requester.post('/driver/auth/phone/login', {
        phone_number: this.phone_object.getE164Format().slice(1),
        verification_code: this.verification_code,
        fcm_token: '',
        platform: 'android',
        version_code: APP_VERSION_CODE,
      }).then((response) => {

        console.log(response);

        switch (response.status) {
          case 'success':
            this.appStore.makeAuth(response.payload);
            break;
        }

      }).catch(e => {
        console.log(e);
      }).finally(() => {
        this.loading = false;
      });

    }
  };

  @computed get phone_is_valid() {
    return this.phone_number.length > 0 && this.phone_object && this.phone_object.isValid();
  }

  render() {
    return (
      <>
        <StatusBar translucent={true} backgroundColor={'rgba(0, 0, 0, 0)'}/>
        <ImageBackground source={require('../images/bg-taxi.jpg')} style={{
          flex: 1,
          resizeMode: 'cover',
          justifyContent: 'flex-end',
        }}>
          <Card style={{margin: size(4), backgroundColor: 'rgba(255, 255, 255, 0.5)'}} elevation={5}>
            <Card.Content>
              <TextInput autoFocus={true} error={this.checked && !this.phone_is_valid} keyboardType={'phone-pad'}
                         mode={'outlined'}
                         disabled={this.loading || ['exists_code_is_sent', 'not_exists_code_is_sent', 'exists_code_was_sent', 'not_exists_code_was_sent'].includes(this.status)}
                         onChangeText={v => this.setPhoneNumber(v)} style={{marginBottom: size(4)}}
                         label="Телефон номер"/>
              {['exists_code_is_sent', 'not_exists_code_is_sent', 'exists_code_was_sent', 'not_exists_code_was_sent'].includes(this.status) && (
                <TextInput autoFocus={true} onChangeText={v => this.setValue('verification_code', v)}
                           keyboardType={'numeric'} mode={'outlined'} style={{marginBottom: size(4)}}
                           label="Тастыктоо коду"/>
              )}
              <Button variant={'secondary'} disabled={this.loading}
                      onPress={() => this.status === '' ? this.sendCode() : this.checkCode()}>
                {['exists_code_is_sent', 'not_exists_code_is_sent', 'exists_code_was_sent', 'not_exists_code_was_sent'].includes(this.status) ? 'Кирүү' : 'Код жиберүү'}
              </Button>
            </Card.Content>
          </Card>
        </ImageBackground>
      </>
    );
  }
}

export default AuthScreen;
