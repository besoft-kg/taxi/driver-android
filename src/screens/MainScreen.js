import React from 'react';
import BaseScreen from './BaseScreen';
import {View, Alert, ToastAndroid, TouchableOpacity, StatusBar} from 'react-native';
import {inject, observer} from 'mobx-react';
import {size} from '../utils';
import {computed, observable} from 'mobx';
import {
  ActivityIndicator,
  Modal,
  Portal,
  Card, Text,
  Title, withTheme,
} from 'react-native-paper';
import requester from '../utils/requester';
import OrdersListComponent from '../components/OrdersListComponent';
import ActiveOrderComponent from '../components/ActiveOrderComponent';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons';
import Sound from 'react-native-sound';
import ToggleSwitch from 'toggle-switch-react-native';
import Button from '../ui/Button';
import RequestingAlarmComponent from '../components/RequestingAlarmComponent';
import TaximeterComponent from "../components/TaximeterComponent";
import BackgroundGeolocation from '@mauron85/react-native-background-geolocation';
import {getSnapshot} from 'mobx-state-tree';

@withTheme
@inject('store') @observer
class MainScreen extends BaseScreen {
  @observable network_status = '';
  @observable busy = false;

  notification = null;
  sound = null;
  requesting_alarm_ref = React.createRef();

  constructor(props) {
    super(props);

    this.notification = new Sound('soft_notification.mp3', Sound.MAIN_BUNDLE, (error) => {
      console.log(error);
    });
    this.notification.setVolume(1);

    this.sound = new Sound('alarm.mp3', Sound.MAIN_BUNDLE, (error) => {
      console.log('error:', error);
    });
    this.sound.setVolume(1);
    this.sound.setCategory('Alarm');

    this.state = {
      visible: false,
    };
  }

  componentDidMount() {
    if (!this.appStore.user.busy) {
      this.subscribeToChannels();
    }

    //this.fetchOrders();
  }

  // fetchOrders = () => {
  //   if (this.busy) return;
  //
  //   this.setValue('busy', true);
  //   requester.get('/driver/order').then((response) => {
  //     if (response.status === 'success') {
  //       this.orderStore.createOrUpdate(response.payload.items);
  //       if (response.payload.active) {
  //         this.orderStore.createOrUpdate(response.payload.active);
  //       }
  //     }
  //   }).catch((e) => {
  //     console.log(e);
  //   }).finally(() => {
  //     this.setValue('busy', false);
  //   });
  // };

  @computed get indicator_color() {
    if (this.network_status === 'connected') {
      return this.props.theme.colors.accent;
    } else if (this.network_status === 'failed') {
      return this.props.theme.colors.error;
    }
    return 'yellow';
  }

  accept = (item) => {
    if (this.busy || this.orderStore.active_order || !this.appStore.user.categories.includes(item.category) || !item.can_be_accepted) {
      return;
    }

    Alert.alert('', 'Заказды кабыл аласызбы?', [
      {
        text: 'Жок',
        style: 'cancel',
      },
      {
        text: 'Ооба', onPress: async () => {
          this.setValue('busy', true);
          try {
            const response = await requester.post('/driver/order/accept', {id: item.id});
            switch (response.status) {
              case 'success':
                this.orderStore.createOrUpdate(response.payload);
                break;
              case 'already_busy':
                ToastAndroid.show('Заказ занят!', ToastAndroid.SHORT);
                break;
            }
          } catch (e) {
            console.log(e);
          } finally {
            this.setValue('busy', false);
          }
        },
      },
    ], {
      cancelable: true,
    });
  };

  subscribeToChannels = () => {
    const {pusher} = this.props;

    const order_channel = pusher.subscribe('private-driver.orders');
    const alarm_channel = pusher.subscribe('private-driver.alarm');

    order_channel.bind('on-create', (data) => {
      this.orderStore.createOrUpdate(data.item);
      this.notification.play((c) => console.log(c));
    });

    order_channel.bind('on-update', (data) => {
      const old_item = getSnapshot(this.orderStore.items.get(data.item.id));
      this.orderStore.createOrUpdate(data.item);
      const item = this.orderStore.items.get(data.item.id);

      if (old_item.status !== 'pending' && item.status === 'pending') {
        this.notification.play((c) => console.log(c));
        setTimeout(() => {
          item.markAsOld();
        }, 1000);
      }

      if (old_item.executor_id === this.appStore.user.id && old_item.status !== item.status) {
        let message = '';

        switch (item.status) {
          case 'cancelled_by_admin':
          case 'cancelled_by_client':
            message = `Заказ ${item.status === 'cancelled_by_admin' ? 'оператор' : 'клиент'} тараптан жетке кагылды`;
            break;
        }

        if (message) Alert.alert('', message, [{}], {cancelable: true});
      }
    });

    order_channel.bind('pusher:subscription_succeeded', (data) => {
      this.setValue('network_status', 'connected');
    });

    alarm_channel.bind('on-create', (data) => {
      this.alarmStore.createOrUpdate(data.item);
    });

    alarm_channel.bind('on-delete', (data) => {
      if (this.requesting_alarm_ref) {
        this.requesting_alarm_ref.stop();
      }
      this.alarmStore.remove(data.item.id);
    });
  };

  setBusy = () => {
    this.appStore.setBusy(!this.appStore.user.busy);

    if (this.appStore.user.busy) {
      setTimeout(() => BackgroundGeolocation.stop(), 1000);
      this.props.pusher.unsubscribe('private-driver.orders');
    } else {
      setTimeout(() => BackgroundGeolocation.start(), 1000);
      this.subscribeToChannels();
    }
  };

  overboardClient = () => {
    if (this.busy || this.appStore.user.busy) return;

    if (!this.orderStore.active_order && !this.taximeterStore.active_item) {
      Alert.alert('', 'Счетчик колдоносузбу?', [
          {
            text: 'Жок',
            style: 'cancel',
          }, {
            text: 'Ооба',
            onPress: async () => {
              try {
                this.setValue('busy', true);
                const response = await requester.post('/driver/taximeter', {
                  latitude: this.appStore.location_lat,
                  longitude: this.appStore.location_lng,
                });
                switch (response.status) {
                  case 'success':
                    this.taximeterStore.createOrUpdate(response.payload);
                    break;
                }
              } catch (e) {
                console.log(e);
              } finally {
                this.setValue('busy', false);
              }
            },
          }],
        {
          cancelable: true,
        });
    } else {
      Alert.alert('', 'Активдүү заказ бар учурда сырттан килент ала албайсыз!', [{}],
        {
          cancelable: true,
        });
    }
  };

  alarmCancel = () => {
    if (!this.appStore.alarm || this.busy) {
      return;
    }

    this.setValue('busy', true);
    requester.delete('/driver/user/alarm', {
      id: this.appStore.alarm.id,
    }).then(response => {
      switch (response.status) {
        case 'success':
          this.appStore.setValue('alarm', null);
          break;
      }
    }).catch(e => console.log(e)).finally(() => {
      this.setValue('busy', false);
    });
  };

  render() {
    const {theme} = this.props;
    const active_alarm = this.alarmStore.active_item;
    const active_taximeter = this.taximeterStore.active_item;

    const component = this.appStore.alarm ? (
      <View style={{
        flex: 1,
        justifyContent: 'space-between',
        padding: size(4),
        paddingTop: StatusBar.currentHeight + size(3),
        backgroundColor: theme.colors.error,
      }}>
        <View flexGrow={1} alignItems={'center'} justifyContent={'center'}>
          <Icons style={{textAlign: 'center'}} color={'red'} flexGrow={1} size={size(70)} name={'alarm-light'}/>
          <Text style={{fontSize: size(12), marginVertical: size(5)}}>Тревога</Text>
          <Text style={{marginBottom: size(10), textAlign: 'center', fontSize: size(6)}}><Text
            style={{fontSize: size(8)}}>Besoft Taxi </Text>
            нин диспетчерине жана Besoft Таксинин айдоочуларына сиздин кайрулууңуз жиберилди!!!
          </Text>
        </View>
        <Button loading={this.busy} disabled={this.busy} onPress={() => this.alarmCancel()} variant={'primary'}>Артка кайтуу</Button>
      </View>
    ) : (
      <>
        {active_alarm && !this.orderStore.active_order && !active_taximeter ? (
          <RequestingAlarmComponent ref={ref => this.requesting_alarm_ref = ref} sound={this.sound}/>
        ) : (
          <View style={{
            flex: 1,
            backgroundColor: theme.colors.background,
          }}>
            <View style={{
              padding: size(4),
              paddingTop: StatusBar.currentHeight + size(3),
              flexDirection: 'row',
              alignItems: 'center',
              backgroundColor: theme.colors.surface,
              justifyContent: 'space-between',
            }}>
              <TouchableOpacity onPress={() => this.setState({visible: true})}>
                <View flex={1} flexDirection={'row'}
                      justifyContent={'flex-start'} alignItems={'center'}>
                  <Icon color={this.indicator_color} size={size(15)}
                        name={'account-circle'}/>
                  <View>
                    <Text style={{marginLeft: size(2), fontSize: 16}}>{this.appStore.user.first_name}</Text>
                    <Text style={{marginLeft: size(2), fontSize: 16}}>{this.appStore.user.last_name}</Text>
                  </View>
                </View>
              </TouchableOpacity>
              <View flex={1} flexDirection={'row'} justifyContent={'flex-end'} alignItems={'center'}>
                <TouchableOpacity
                  size={20}
                  style={{
                    marginRight: size(4),
                    padding: size(2),
                    borderRadius: 8,
                    backgroundColor: theme.colors.surface,
                  }}
                  disabled={this.busy}
                  onPress={() => this.overboardClient()}>
                  <View flex={1} flexDirection={'row'} justifyContent={'flex-start'} alignItems={'center'}>
                    <Icons color={theme.colors.text} size={35} name={'hail'}/>
                    <Icons color={theme.colors.text} size={20} name={'taxi'}/>
                  </View>
                </TouchableOpacity>
                <ToggleSwitch
                  isOn={!this.appStore.user.busy}
                  onColor={theme.colors.accent}
                  offColor={'#a60202'}
                  size={'large'}
                  onToggle={() => !this.orderStore.active_order && !active_taximeter ? this.setBusy() : null}
                />
              </View>
            </View>
            <View flexGrow={1} flex={1} style={{margin: size(4)}}>
              {this.busy ? (
                <View flex={1} alignItems={'center'} justifyContent={'center'}>
                  <ActivityIndicator color={theme.colors.accent} size={size(14)}/>
                </View>
              ) : (
                <>
                  {this.orderStore.active_order ? (
                    <ActiveOrderComponent/>
                  ) : (
                    <>
                      {active_taximeter ? (
                        <TaximeterComponent
                          busy={this.busy}
                          onBusy={() => this.setValue('busy', true)}
                          onFree={() => this.setValue('busy', false)}
                        />
                      ) : (
                        <>
                          <View justifyContent={'space-between'} alignItems={'center'} flexDirection={'row'}>
                            <Title>Заказдар</Title>
                          </View>
                          {!this.appStore.user.busy && (
                            <OrdersListComponent accept={this.accept}/>
                          )}
                        </>
                      )}
                    </>
                  )}
                </>
              )}
            </View>
          </View>
        )}
      </>
    );

    return (
      <>
        <StatusBar translucent={true} barStyle={theme.dark ? 'light-content' : 'dark-content'}
                   backgroundColor={'rgba(0, 0, 0, 0)'}/>

        <Portal>
          <Modal visible={this.state.visible} onDismiss={() => this.setState({visible: false})}>
            <Card style={{margin: size(4)}}>
              <Card.Content style={{margin: size(2)}}>
                <Title>Аты жөнү: <Text style={{fontSize: size(6)}}>{this.appStore.user.full_name}</Text></Title>
                <Title>Баланс: <Text style={{fontSize: size(6)}}>{this.appStore.user.balance} сом</Text></Title>
                <Title>Тел.номур: <Text style={{fontSize: size(6)}}>+{this.appStore.user.phone_number}</Text></Title>
                <Title>Гос.номур: <Text style={{fontSize: size(6)}}>{this.appStore.user.state_reg_plate}</Text></Title>
              </Card.Content>
            </Card>
          </Modal>
        </Portal>

        {component}
      </>
    );
  }
}

export default MainScreen;
