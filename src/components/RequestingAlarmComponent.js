import BaseComponent from './BaseComponent';
import React from 'react';
import {inject, observer} from 'mobx-react';
import {size} from '../utils';
import {Linking, StatusBar, View, Text, BackHandler} from 'react-native';
import Button from '../ui/Button';
import {withTheme} from 'react-native-paper';
import {observable} from 'mobx';

@withTheme
@inject('store') @observer
class RequestingAlarmComponent extends BaseComponent {
  timer_id = null;
  @observable timer = 0;

  call = () => {
    const {driver} = this.alarmStore.active_item;
    if (!driver) {
      return;
    }
    this.props.sound.stop();
    Linking.openURL(`tel:+${driver.phone_number}`);
  };

  openMap = () => {
    const {active_item} = this.alarmStore;
    this.props.sound.stop();
    Linking.openURL(`geo:${active_item.address_lat},${active_item.address_lng}`).then(console.log).catch(console.log);
  };

  reject = () => {
    const {active_item} = this.alarmStore;
    if (active_item) {
      active_item.setValue('_cancelled', true);
    }
    this.props.sound.stop();
  };

  stop = () => {
    this.props.sound.stop();
    if (this.timer_id) {
      clearInterval(this.timer_id);
    }
  };

  componentDidMount() {
    if (!this.alarmStore.active_item) {
      return;
    }

    this.props.sound.play((c) => console.log(c));

    this.timer_id = setInterval(() => {
      this.setValue('timer', Math.floor((new Date() - this.alarmStore.active_item.created_at) / 1000));
      if (this.timer >= 20) {
        clearInterval(this.timer_id);
        this.reject();
      }
    }, 1000);

    BackHandler.addEventListener('hardwareBackPress', () => {
      if (this.timer_id) {
        clearInterval(this.timer_id);
      }
      this.reject();
      return true;
    });
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress');
    if (this.timer_id) {
      clearInterval(this.timer_id);
    }
    this.reject();
  }

  render() {
    const {theme} = this.props;

    return (
      <View style={{
        flex: 1,
        justifyContent: 'space-between',
        padding: size(4),
        paddingTop: StatusBar.currentHeight + size(3),
        backgroundColor: theme.colors.error,
      }}>
        <View>

        </View>
        <View>
          <Button
            loading={this.busy}
            disabled={this.busy}
            onPress={() => this.reject()}
            variant={'secondary'}
            style={{marginBottom: size(4)}}
          >Четке кагуу <Text style={{color: 'red'}}>{20 - this.timer}</Text></Button>
          <Button
            loading={this.busy}
            disabled={this.busy}
            onPress={() => this.call()}
            variant={'secondary'}
            style={{marginBottom: size(4)}}
          >Чалуу</Button>
          <Button
            loading={this.busy}
            disabled={this.busy}
            onPress={() => this.openMap()}
            variant={'primary'}
          >Картадан көрүү</Button>
        </View>
      </View>
    );
  }
}

export default RequestingAlarmComponent;
