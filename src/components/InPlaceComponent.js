import React from 'react';
import BaseComponent from './BaseComponent';
import {View} from 'react-native';
import {inject, observer} from 'mobx-react';
import {computed} from 'mobx';
import requester from '../utils/requester';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Button from '../ui/Button';
import PassengerInfoComponent from './PassengerInfoComponent';
import OrderButtonsComponent from './OrderButtonsComponent';
import {size} from '../utils';

@inject('store') @observer
class InPlaceComponent extends BaseComponent {
  @computed get active_order() {
    return this.orderStore.active_order;
  }

  goingWithClient = () => {
    if (this.props.busy) {
      return;
    }
    this.props.onBusy();
    requester.post('/driver/order/going_with_client', {
      id: this.active_order.id,
    }).then(response => {
      console.log(response);
      switch (response.status) {
        case 'success':
          this.taximeterStore.createOrUpdate(response.payload.taximeter);
          this.orderStore.createOrUpdate(response.payload.order);
          this.orderStore.active_order.setValue('taximeter', response.payload.taximeter.id);
          break;
      }
    }).catch(e => console.log(e)).finally(() => {
      this.props.onFree();
    });
  };

  render() {
    const item = this.active_order;

    return (
      <View flex={1} justifyContent={'flex-end'}>
        <PassengerInfoComponent item={item}/>
        <View style={{marginVertical: size(4)}}>
          <Button
            onPress={() => this.goingWithClient()}
            disabled={item.distance >= 1000}
            variant={'primary'}
            icon={props => <Icon {...props} size={30} name={'airplane-takeoff'}/>}
            mode={'outlined'}>Кеттик!</Button>
        </View>
        <OrderButtonsComponent
          onBusy={this.props.onBusy}
          onFree={this.props.onFree}
          busy={this.props.busy}
          call fakeCall map/>
      </View>
    );
  }
}

export default InPlaceComponent;
