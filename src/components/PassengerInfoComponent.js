import React from 'react';
import BaseComponent from './BaseComponent';
import {Linking, View} from 'react-native';
import {List, Card, withTheme} from 'react-native-paper';

@withTheme
class PassengerInfoComponent extends BaseComponent {
  call = () => {
    const {item} = this.props;
    Linking.openURL(`tel:+${item.phone_number || item.client.phone_number}`);
  };

  openMap = () => {
    const {item} = this.props;
    Linking.openURL(`geo:${item.address_lat},${item.address_lng}`).then(console.log).catch(console.log);
  };

  render() {
    const {item} = this.props;

    return (
      <View>
        <Card>
          <Card.Content>
            {item.client && (
              <List.Item
                titleStyle={{fontSize: 20}}
                title={item.client.full_name}
                description="Кардар аты жөнү"
                left={props => <List.Icon {...props} icon="account" />}
              />
            )}
            <List.Item
              titleStyle={{fontSize: 20}}
              onPress={() => this.call()}
              title={`+${item.phone_number || item.client.phone_number}`}
              description="Кардар менен байланышуу"
              left={props => <List.Icon {...props} icon="phone" />}
            />
            <List.Item
              titleStyle={{fontSize: 20}}
              titleNumberOfLines={6}
              onPress={() => this.openMap()}
              title={item.address}
              description="Кардарды карта аркылуу көрүү"
              left={props => <List.Icon {...props} icon="map-marker" />}
            />
            <List.Item
              titleStyle={{fontSize: 20}}
              onPress={() => this.openMap()}
              title={item.distance_text && item.distance_text}
              description="Кардарды карта аркылуу көрүү"
              left={props => <List.Icon {...props} icon="flag-triangle" />}
            />
          </Card.Content>
        </Card>
      </View>
    );
  }
}

export default PassengerInfoComponent;
