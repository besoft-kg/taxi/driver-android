import BaseComponent from './BaseComponent';
import {size} from '../utils';
import React from 'react';
import {TouchableOpacity, View, Alert} from 'react-native';
import {Card, withTheme, Text} from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {inject, observer} from 'mobx-react';
import {observable} from "mobx";

@withTheme
@inject('store') @observer
class OrderItemComponent extends BaseComponent {
  timer_id = null;
  @observable difference_time = 0;
  @observable time = -1;

  componentDidMount() {
    const {item} = this.props;

    if (item.distance >= 2000 && item.difference_time <= 20) {
      this.timer_id = setInterval(() => {
        this.setValue('difference_time', item.difference_time);

        if (item.distance > 5000) this.setValue('time', 19);
        else if (item.distance > 2000) this.setValue('time', 9);

        if (item.difference_time > this.time) {
          clearInterval(this.timer_id);
           this.timer_id = null;
          this.setValue('time', -1);
        }
      }, 1000);
    }
  }

  componentWillUnmount() {
    clearInterval(this.timer_id);
  }

  accept = (item) => {
    if (this.timer_id) {
      Alert.alert(
        `Заказды ${this.time - this.difference_time} секундтан соң ала аласыз`,
        `Сиз ${item.distance_text} алыстыктасыз`,
        [{}], {cancelable: true}
        );
    } else this.props.accept(item);
  };

  render() {
    const {item, theme} = this.props;
    return (
      <TouchableOpacity onPress={() => this.accept(item)} style={{marginBottom: size(3)}}>
        <Card style={{backgroundColor: item.is_new  ? theme.colors.accent : theme.colors.surface}}>
          <Card.Content>
            <View flex={1} flexDirection={'row'} alignItems={'center'} justifyContent={'space-between'}>
              <Text>
                {item.address ? <Text style={{
                  marginBottom: size(2), fontSize: 18, fontWeight: 'bold',
                }}>{item.address.toUpperCase()}</Text> : null}
              </Text>
              {this.time - this.difference_time > -1 ? (
                <Text alignItems={'flex-end'} style={{backgroundColor: this.time > 0 && '#D98880', paddingHorizontal: 10, borderRadius: 6}}>
                  <Text style={{color: this.time - this.difference_time > 0 ? 'red' : 'green'}}>{this.time - this.difference_time}</Text>
                </Text>
              ) : null}
            </View>
            <View flex={1} flexDirection={'row'} alignItems={'center'} justifyContent={'space-between'}>
              <Text>
                <Icon style={{
                  fontSize: 26,
                  color: theme.colors.text,
                }} name={'flight-takeoff'}/>
              </Text>
              {item.ui_category}
              {item.distance_text && (
                <Text>{item.distance_text}</Text>
              )}
            </View>
          </Card.Content>
        </Card>
      </TouchableOpacity>
    );
  }
}

export default OrderItemComponent;
