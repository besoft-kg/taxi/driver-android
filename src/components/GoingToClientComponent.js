import React from 'react';
import BaseComponent from './BaseComponent';
import {View, Linking, Alert, Image, BackHandler} from 'react-native';
import {inject, observer} from 'mobx-react';
import {computed} from 'mobx';
import requester from '../utils/requester';
import {size} from '../utils';
import PassengerInfoComponent from './PassengerInfoComponent';
import OrderButtonsComponent from './OrderButtonsComponent';
import Button from '../ui/Button';

@inject('store') @observer
class GoingToClientComponent extends BaseComponent {
  call = () => {
    Linking.openURL(`tel:+${this.active_order.phone_number}`);
  };

  @computed get active_order() {
    return this.orderStore.active_order;
  }

  inPlace = () => {
    if (this.props.busy) {
      return;
    }

    if (this.active_order.distance >= 1000) {
      Alert.alert('', 'Клиентке 1 километрден аз аралык калган учурда басыңыз!', [{}], {cancelable: true});
      return;
    }

    this.props.onBusy();
    requester.post('/driver/order/in_place', {
      id: this.active_order.id,
    }).then(response => {
      switch (response.status) {
        case 'success':
          this.orderStore.createOrUpdate(response.payload);
          break;
      }
    }).catch(e => console.log(e)).finally(() => {
      this.props.onFree();
    });
  };

  openMap = (order) => {
    Linking.openURL(`geo:${order.address_lat},${order.address_lng}`).then(console.log).catch(console.log);
  };

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", () => {
      this.cancel();
      return true;
    });
  }

  render() {
    const item = this.active_order;

    return (
      <View flex={1} justifyContent={'flex-end'}>
        <PassengerInfoComponent openMap={() => this.openMap(item)} call={() => this.call()} item={item}/>
        <View flexDirection={'row'} style={{marginVertical: size(4)}}>
          <Button variant={'primary'} flexGrow={1} onPress={() => this.inPlace()} icon={'map-marker'}>Дарекке
            келдим</Button>
        </View>
        <OrderButtonsComponent busy={this.props.busy} onBusy={this.props.onBusy} onFree={this.props.onFree} call cancel item={item} map/>
      </View>
    );
  }
}

export default GoingToClientComponent;
