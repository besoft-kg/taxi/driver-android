import React from 'react';
import BaseComponent from './BaseComponent';
import {Text, withTheme} from 'react-native-paper';
import {alarmAnnouncement, size} from '../utils';
import {Alert, Linking, TouchableOpacity, View} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {inject, observer} from 'mobx-react';
import requester from '../utils/requester';

@withTheme
@inject('store') @observer
class OrderButtonsComponent extends BaseComponent {
  call = (tel, plus = true) => {
    Linking.openURL(`tel:${plus ? '+' : ''}${tel}`);
  };

  cancel = () => {
    if (this.props.busy) {
      return;
    }

    Alert.alert('', 'Заказды жокко чыгарууга оюңуз бекемби?', [{
      text: 'Жок',
      style: 'cancel',
    },
      {
        text: 'Ооба', onPress: () => {
          if (this.props.busy) {
            return;
          }
          this.props.onBusy();
          requester.post('/driver/order/cancel', {
            id: this.orderStore.active_order.id,
          }).then(response => {
            switch (response.status) {
              case 'success':
                this.orderStore.createOrUpdate(response.payload);
                break;
            }
          }).catch(e => console.log(e)).finally(() => {
            this.props.onFree();
          });
        },
      }]);
  };

  fakeCall = () => {
    if (this.props.busy) {
      return;
    }

    Alert.alert('Жалган кабар', 'Оюңуз бекемби?', [
      {
        text: 'Жок',
        style: 'cancel',
      },
      {
        text: 'Ооба', onPress: async () => {
          this.props.onBusy();
          requester.post('/driver/order/fake_call', {
            id: this.orderStore.active_order.id,
          }).then(response => {
            switch (response.status) {
              case 'success':
                this.orderStore.remove(response.payload.id);
                break;
            }
          }).catch(e => console.log(e)).finally(() => {
            this.props.onFree();
          });
        },
      },
    ], {
      cancelable: true,
    });
  };

  alarm = () => {
    if (this.props.busy) {
      return;
    }
    alarmAnnouncement({onFree: this.props.onFree, onBusy: this.props.onBusy});
  };

  openMap = (order = null) => {
    if (!order) Linking.openURL(`geo:`).then(console.log).catch(console.log);
    else Linking.openURL(`geo:${order.address_lat},${order.address_lng}`).then(console.log).catch(console.log);
  };

  render() {
    const item = this.orderStore.active_order;

    const {theme} = this.props;

    return (
      <>
        <View justifyContent={'space-around'} alignItems={'flex-start'} flexDirection={'row'}
              style={{marginTop: size(2)}}>

          {this.props.map && (
            <>
              {item && item.address_lat && item.address_lng ? (
                <TouchableOpacity onPress={() => this.openMap(item)}>
                  <Icon style={{textAlign: 'center', color: theme.colors.accent}} flexGrow={1} size={30} name={'map'}/>
                  <Text style={{fontSize: 16, textAlign: 'center', color: theme.colors.accent}}>Карта</Text>
                </TouchableOpacity>
              ) : (
                <TouchableOpacity onPress={() => this.openMap()}>
                  <Icon style={{textAlign: 'center', color: theme.colors.accent}} flexGrow={1} size={30} name={'map'}/>
                  <Text style={{fontSize: 16, textAlign: 'center', color: theme.colors.accent}}>Карта</Text>
                </TouchableOpacity>
              )}
            </>
          )}

          {this.props.call && item && (
            <TouchableOpacity onPress={() => this.call(item.phone_number || item.client.phone_number)}>
              <Icon style={{textAlign: 'center', color: theme.colors.primary}} flexGrow={1} size={30} name={'phone'}/>
              <Text style={{fontSize: 16, textAlign: 'center', color: theme.colors.primary}}>Чалуу</Text>
            </TouchableOpacity>
          )}

          {this.props.cancel && item && (
            <TouchableOpacity onPress={() => this.cancel()}>
              <Icon style={{textAlign: 'center', color: theme.colors.error}} flexGrow={1} size={30} name={'cancel'}/>
              <Text style={{fontSize: 16, textAlign: 'center', color: theme.colors.error}}>Жокко</Text>
              <Text style={{fontSize: 16, textAlign: 'center', color: theme.colors.error}}>чыгаруу</Text>
            </TouchableOpacity>
          )}

          {this.props.fakeCall && item && (
            <TouchableOpacity onPress={() => this.fakeCall()}>
              <Icon style={{textAlign: 'center', color: theme.colors.error}} flexGrow={1} size={30}
                    name={'account-multiple-remove-outline'}/>
              <Text style={{fontSize: 16, textAlign: 'center', color: theme.colors.error}}>Жалган</Text>
              <Text style={{fontSize: 16, textAlign: 'center', color: theme.colors.error}}>кабар</Text>
            </TouchableOpacity>
          )}

          {this.props.ambulance && (
            <TouchableOpacity onPress={() => this.call('103', false)}>
              <Icon style={{textAlign: 'center'}} color={'red'} flexGrow={1} size={30} name={'alert-decagram-outline'}/>
              <Text style={{fontSize: 16, color: theme.colors.error, textAlign: 'center'}}>Тез жардам</Text>
            </TouchableOpacity>
          )}

          {this.props.alarm && (
            <View justifyContent={'space-around'} alignItems={'flex-start'} flexDirection={'row'}>
              <TouchableOpacity onPress={() => this.alarm()}>
                <Icon style={{textAlign: 'center'}} color={'red'} flexGrow={1} size={30} name={'alarm-light'}/>
                <Text style={{fontSize: 16, color: theme.colors.error, textAlign: 'center'}}>Тревога</Text>
              </TouchableOpacity>
            </View>
          )}

        </View>
      </>
    );
  }
}

export default OrderButtonsComponent;
