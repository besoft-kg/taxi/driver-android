import React from 'react';
import BaseComponent from './BaseComponent';
import {View, Linking} from 'react-native';
import {inject, observer} from 'mobx-react';
import {computed} from 'mobx';
import {Text, Card} from 'react-native-paper';
import Button from "../ui/Button";
import {alarmAnnouncement, size} from '../utils';
import OrderButtonsComponent from './OrderButtonsComponent';

@inject('store') @observer
class CompleteOrderComponent extends BaseComponent {
  call = () => {
    Linking.openURL(`tel:${103}`);
  };

  @computed get active_order() {
    return this.orderStore.active_order;
  }

  complete = () => {
    if (this.props.busy) {
      return;
    }

    this.active_order.taximeter.setValue('fully_completed', true);
    this.active_order.setValue('fully_completed', true)
  };

  alarm = () => {
    if (this.props.busy) {
      return;
    }
    alarmAnnouncement({onBusy: this.props.onBusy, onFree: this.props.onFree});
  };

  render = () => {
    const item = this.orderStore.active_order;
    const taximeter = item.taximeter;

    return (
      <>
        <View flex={1} justifyContent={'flex-end'}>

          <Card>
            <Card.Content>
              <View alignItems={'center'}>
                <Text style={{fontSize: 64}}>{taximeter.amount_to_pay} c</Text>
                <Text style={{fontSize: 38}}>{taximeter.ui_distance}</Text>
              </View>
              <View flexDirection={'row'} justifyContent={'space-between'} alignItems={'center'}>
                <Text style={{fontSize: 18, color: '#808080'}}>{item.category.title}</Text>
                <Text style={{fontSize: 24, color: '#fff'}}>{item.category.landing_price} c / {item.category.price_for_km} c</Text>
              </View>
              <View flexDirection={'row'} justifyContent={'space-between'} alignItems={'center'}>
                <Text style={{fontSize: 18, color: '#808080'}}>Дистанция</Text>
                <Text style={{fontSize: 24, color: '#fff'}}>{taximeter.ui_distance} ({taximeter.amount_to_pay - item.category.landing_price} c)</Text>
              </View>
              <View flexDirection={'row'} justifyContent={'space-between'} alignItems={'center'}>
                <Text style={{fontSize: 18, color: '#808080'}}>Күтүү</Text>
                <Text style={{fontSize: 24, color: '#fff'}}>{taximeter.ui_waiting_time} ({taximeter.ui_waiting_time_price})</Text>
              </View>
            </Card.Content>
          </Card>

          <View style={{marginVertical: size(4)}}>
            <Button variant={'danger'} style={{marginBottom: size(4)}} onPress={() => this.alarm()} icon={'alarm-light'}>Тревога</Button>
            <Button variant={'primary'} onPress={() => this.complete()} icon={'check'}>Артка кайтуу</Button>
          </View>
        </View>

        <OrderButtonsComponent busy={this.props.busy} onBusy={this.props.onBusy} onFree={this.props.onFree} map ambulance alarm item={item}/>
      </>
    );
  };
}

export default CompleteOrderComponent;
