import React from 'react';
import BaseComponent from './BaseComponent';
import {View, Alert} from 'react-native';
import {inject, observer} from 'mobx-react';
import {computed} from 'mobx';
import requester from '../utils/requester';
import {Text, Card} from 'react-native-paper';
import Button from "../ui/Button";
import {size} from '../utils';
import OrderButtonsComponent from './OrderButtonsComponent';

@inject('store') @observer
class GoingWithClientComponent extends BaseComponent {
  @computed get active_order() {
    return this.orderStore.active_order;
  }

  complete = () => {
    if (this.props.busy) {
      return;
    }

    Alert.alert('Аяктоо', 'Оюңуз бекемби?', [
      {
        text: 'Жок',
        style: 'cancel',
      },
      {
        text: 'Аяктоо', onPress: async () => {
          this.props.onBusy();
          requester.post('/driver/order/complete', {
            id: this.active_order.id,
          }).then(response => {
            switch (response.status) {
              case 'success':
                console.log(response);
                this.orderStore.createOrUpdate({
                  ...response.payload.order,
                  taximeter: response.payload.taximeter,
                });
                break;
            }
          }).catch(e => console.log(e)).finally(() => {
            this.props.onFree();
          });
        },
      }
    ], {
      cancelable: true,
    });
  };

  wait = () => {
    this.taximeterStore.active_item.startWaiting();
  };

  render() {
    const item = this.orderStore.active_order;
    const taximeter = item.taximeter;

    return (
      <>
        <View flex={1} justifyContent={'flex-end'}>

          <Card>
            <Card.Content>
              <View alignItems={'center'}>
                <Text style={{fontSize: 64}}>{taximeter.amount_to_pay} c</Text>
                <Text style={{fontSize: 38}}>{taximeter.ui_distance}</Text>
              </View>

              <View flexDirection={'row'} style={{marginVertical: size(2)}} justifyContent={'space-between'} alignItems={'center'}>
                <Text style={{fontSize: 18, color: '#808080'}}>{item.category.title}</Text>
                <Text style={{fontSize: 24, color: '#fff'}}>{item.category.landing_price} c / {item.category.price_for_km} c</Text>
              </View>
              <View flexDirection={'row'} style={{marginBottom: size(2)}} justifyContent={'space-between'} alignItems={'center'}>
                <Text style={{fontSize: 18, color: '#808080'}}>Дистанция</Text>
                <Text style={{fontSize: 24, color: '#fff'}}>{taximeter.ui_distance} ({taximeter.amount_to_pay - item.category.landing_price - taximeter.waiting_time_price} c)</Text>
              </View>
              <View flexDirection={'row'} justifyContent={'space-between'} alignItems={'center'}>
                <Text style={{fontSize: 18, color: '#808080'}}>Күтүү</Text>
                <Text style={{fontSize: 24, color: '#fff'}}>{taximeter.ui_waiting_time} ({taximeter.ui_waiting_time_price})</Text>
              </View>
            </Card.Content>
          </Card>

          <View style={{marginVertical: size(4)}}>
            <Button variant={'secondary'} loading={taximeter._waiting_interval_id} disabled={taximeter._waiting_interval_id} style={{marginBottom: size(4)}} onPress={() => this.wait()} icon={'timer-sand'}>Күтүү</Button>
            <Button variant={'primary'} onPress={() => this.complete()} icon={'check'}>Заказды аяктоо</Button>
          </View>
        </View>

        <OrderButtonsComponent onBusy={this.props.onBusy} onFree={this.props.onFree} item={item} busy={this.props.busy} map ambulance alarm/>
      </>
    );
  }
}

export default GoingWithClientComponent;
