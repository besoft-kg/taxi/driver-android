import React from 'react';
import BaseComponent from './BaseComponent';
import {FlatList} from 'react-native';
import {inject, observer, Observer} from 'mobx-react';
import {observable, values} from 'mobx';
import OrderItemComponent from './OrderItemComponent';

@inject('store') @observer
class OrdersListComponent extends BaseComponent {
  @observable timer = true;
  timer_id = null;

  componentDidMount() {
    this.timer_id = setInterval(() => {
      this.setValue('timer', !this.timer);
    }, 1000);
  }

  componentWillUnmount() {
    clearInterval(this.timer_id);
  }

  render() {
    return (
      <FlatList
        renderItem={({item}) => (
          <Observer>
            {() => (
               <OrderItemComponent accept={this.props.accept} item={item} />
            )}
          </Observer>
        )}
        data={values(this.orderStore.items).filter(v => v.status === 'pending').sort((a, b) => b.created_at - a.created_at)}
      />
    );
  }
}

export default OrdersListComponent;
