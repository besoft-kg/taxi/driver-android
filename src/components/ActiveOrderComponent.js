import React from 'react';
import BaseComponent from './BaseComponent';
import {inject, observer} from 'mobx-react';
import GoingToClientComponent from './GoingToClientComponent';
import {ActivityIndicator} from 'react-native-paper';
import {observable} from 'mobx';
import {View} from 'react-native';
import InPlaceComponent from './InPlaceComponent';
import GoingWithClientComponent from './GoingWithClientComponent';
import CompleteOrderComponent from "./CompleteOrderComponent";

@inject('store') @observer
class ActiveOrderComponent extends BaseComponent {
  @observable busy = false;

  render() {
    const item = this.orderStore.active_order;

    if (this.busy) {
      return (
        <View flex={1} justifyContent={'center'}>
          <ActivityIndicator/>
        </View>
      );
    }

    let component = null;

    const props = {
      busy: this.busy,
      onBusy: () => this.setValue('busy', true),
      onFree: () => this.setValue('busy', false),
    };

    switch (item.status) {
      case 'going_to_client':
        component = (
          <GoingToClientComponent {...props} />
        );
        break;
      case 'in_place':
        component = (
          <InPlaceComponent {...props} />
        );
        break;
      case 'going_with_client':
        component = (
          <GoingWithClientComponent {...props} />
        );
        break;
      case 'completed':
        component = (
          <CompleteOrderComponent {...props} />
        );
        break;
    }

    return component;
  }
}

export default ActiveOrderComponent;
