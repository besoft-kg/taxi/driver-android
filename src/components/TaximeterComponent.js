import React from 'react';
import BaseComponent from './BaseComponent';
import {View, Alert} from 'react-native';
import {inject, observer} from 'mobx-react';
import {computed} from 'mobx';
import requester from '../utils/requester';
import {Text, Card} from 'react-native-paper';
import Button from '../ui/Button';
import {size} from '../utils';
import OrderButtonsComponent from './OrderButtonsComponent';

@inject('store') @observer
class TaximeterComponent extends BaseComponent {
  @computed get active() {
    return this.taximeterStore.active_item;
  }

  complete = () => {
    if (this.props.busy) {
      return;
    }

    if (this.taximeterStore.active_item.ended_at) {
      this.taximeterStore.active_item.setValue('fully_completed', true);
    } else {
      Alert.alert('Аяктоо', 'Оюңуз бекемби?', [
        {
          text: 'Жок',
          style: 'cancel',
        },
        {
          text: 'Аяктоо', onPress: async () => {
            this.props.onBusy();
            requester.post('/driver/taximeter/complete', {
              id: this.taximeterStore.active_item.id,
            }).then(response => {
              console.log(response);
              switch (response.status) {
                case 'success':
                  this.taximeterStore.active_item.stopWaiting();
                  this.taximeterStore.createOrUpdate(response.payload);
                  break;
              }
            }).catch(e => console.log(e)).finally(() => {
              this.props.onFree();
            });
          },
        },
      ], {
        cancelable: true,
      });
    }
  };

  wait = () => {
    this.active.startWaiting();
  };

  render() {
    const item = this.active;
    const category = this.appStore.user.categories[0];

    if (!item.fully_completed && item.ended_at) {
      return (
        <>
          <View flex={1} justifyContent={'flex-end'}>
            <Card>
              <Card.Content>
                <View alignItems={'center'}>
                  <Text style={{fontSize: 64}}>{item.amount_to_pay} c</Text>
                  <Text style={{fontSize: 38}}>{item.ui_distance}</Text>
                </View>
                <View flexDirection={'row'} justifyContent={'space-between'} alignItems={'center'}>
                  <Text style={{fontSize: 18, color: '#808080'}}>{category.title}</Text>
                  <Text style={{fontSize: 24, color: '#fff'}}>{category.landing_price} c
                    / {category.price_for_km} c</Text>
                </View>
                <View flexDirection={'row'} justifyContent={'space-between'} alignItems={'center'}>
                  <Text style={{fontSize: 18, color: '#808080'}}>Дистанция</Text>
                  <Text style={{
                    fontSize: 24,
                    color: '#fff',
                  }}>{item.ui_distance} ({item.amount_to_pay - category.landing_price} c)</Text>
                </View>
                <View flexDirection={'row'} justifyContent={'space-between'} alignItems={'center'}>
                  <Text style={{fontSize: 18, color: '#808080'}}>Күтүү</Text>
                  <Text
                    style={{fontSize: 24, color: '#fff'}}>{item.ui_waiting_time} ({item.ui_waiting_time_price})</Text>
                </View>
              </Card.Content>
            </Card>

            <View style={{marginVertical: size(4)}}>
              <Button variant={'danger'} loading={item._waiting_interval_id} disabled={item._waiting_interval_id}
                      style={{marginBottom: size(4)}} onPress={() => this.alarm()} icon={'timer-sand'}>Тревога</Button>
              <Button variant={'primary'} onPress={() => this.complete()} icon={'check'}>Аяктоо</Button>
            </View>
          </View>

          <OrderButtonsComponent
            onBusy={this.props.onBusy}
            onFree={this.props.onFree}
            map ambulance alarm
            item={item}/>
        </>
      );
    }

    return (
      <>
        <View flex={1} justifyContent={'space-between'}>

          <Card>
            <Card.Content>
              <View alignItems={'center'}>
                <Text style={{fontSize: 64}}>{item.amount_to_pay} c</Text>
                <Text style={{fontSize: 38}}>{item.ui_distance}</Text>
              </View>
            </Card.Content>
          </Card>

          <Card style={{padding: size(1)}}>
            <Card.Content>
              <View flexDirection={'row'} justifyContent={'space-between'} alignItems={'center'}>
                <Text style={{fontSize: 18, color: '#808080'}}>{category.title}</Text>
                <Text style={{fontSize: 24, color: '#fff'}}>{category.landing_price} c
                  / {category.price_for_km} c</Text>
              </View>
              <View flexDirection={'row'} justifyContent={'space-between'} alignItems={'center'}>
                <Text style={{fontSize: 18, color: '#808080'}}>Дистанция</Text>
                <Text style={{
                  fontSize: 24,
                  color: '#fff',
                }}>{item.ui_distance} ({item.amount_to_pay - category.landing_price} c)</Text>
              </View>
              <View flexDirection={'row'} justifyContent={'space-between'} alignItems={'center'}>
                <Text style={{fontSize: 18, color: '#808080'}}>Күтүү</Text>
                <Text style={{fontSize: 24, color: '#fff'}}>{item.ui_waiting_time} ({item.ui_waiting_time_price})</Text>
              </View>
            </Card.Content>
          </Card>

          <View style={{marginVertical: size(4)}}>
            <Button variant={'secondary'} loading={item._waiting_interval_id} disabled={item._waiting_interval_id}
                    style={{marginBottom: size(4)}} onPress={() => this.wait()} icon={'timer-sand'}>Күтүү</Button>
            <Button variant={'primary'} onPress={() => this.complete()} icon={'check'}>Аяктоо</Button>
          </View>
        </View>

        <OrderButtonsComponent
          onBusy={this.props.onBusy}
          onFree={this.props.onFree}
          map ambulance alarm cancel
          item={item}/>
      </>
    );
  }
}

export default TaximeterComponent;
