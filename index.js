import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';
import Root from './src/Root';
import messaging from '@react-native-firebase/messaging';

console.disableYellowBox = true;

messaging().setBackgroundMessageHandler(async remoteMessage => {
  console.log('Message handled in the background!', remoteMessage);
});

AppRegistry.registerComponent(appName, () => Root);
